== Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Condimentos

* Mantequilla
* Sal

=== Aperitivos

* Anacardos
* Frutos secos
* Kikos
* Patatas fritas
* Pistachos
* Salchichón

=== Platos principales

* Carrillá
* Chuletas de cabeza
* Costillas de cerdo
* Patatas para asar
* Verduras para asar

=== Postres

* Arroz con leche
* Flan
* Mandarinas
